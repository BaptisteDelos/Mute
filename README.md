# Mute

Let there be sound.

## Dependencies

Mute relies on the **Qt** and **OpenCV** libraries.
The project building requires at least CMake 3.13.2 version.

## Build instructions

We invite you to follow the usual build practices :

```
mkdir build && cd build
cmake ..
make -j
```

Project executable will be accessible in the *PROJECT_DIR/build/bin* folder.
