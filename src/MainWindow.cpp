#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QFileDialog>
#include <QString>

#include <iostream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(QString::fromStdString("Mute"));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_record_pushButton_clicked()
{
    m_recorder.record();

//    VideoCapture cap;
//    TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
//    Size subPixWinSize(10,10), winSize(31,31);

//    const int MAX_COUNT = 500;
//    bool needToInit = true;
//    bool nightMode = false;

//    String videoName(m_filename);
//    destroyWindow(videoName);

//    namedWindow(m_filename, 1);
////    setMouseCallback( "LK Demo", onMouse, 0 );

//    Mat gray, prevGray, frame;
//    std::vector<Point2f> points[2];

//    cap.open(videoName);

//    if (!cap.isOpened()) {
//        std::cout << "Could not initialize video capture" << std::endl;
//        return;
//    }

//    for(;;) {

//        cap >> frame;
//        if (frame.empty())
//            break;

//        frame.copyTo(m_currentFrame);
//        cvtColor(m_currentFrame, gray, COLOR_BGR2GRAY);

//        if (nightMode)
//            m_currentFrame = Scalar::all(0);

//        if (needToInit)
//        {
//            // automatic initialization
//            goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
//            cornerSubPix(gray, points[1], subPixWinSize, Size(-1,-1), termcrit);
//            m_addRemovePt = false;
//        }
//        else if( !points[0].empty() )
//        {
//            std::vector<uchar> status;
//            std::vector<float> err;

//            if (prevGray.empty())
//                gray.copyTo(prevGray);

//            //
//            calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize,
//                                 3, termcrit, 0, 0.001);
//            size_t i, k;
//            for (i = k = 0; i < points[1].size(); ++i)
//            {
//                if (m_addRemovePt) {
//                    if(norm(m_point - points[1][i]) <= 5) {
//                        m_addRemovePt = false;
//                        continue;
//                    }
//                }
//                if(!status[i])
//                    continue;

//                points[1][k++] = points[1][i];
//                circle(m_currentFrame, points[1][i], 3, Scalar(0,255,0), -1, 8);
//            }
//            points[1].resize(k);
//        }

//        if (m_addRemovePt && points[1].size() < (size_t)MAX_COUNT) {
//            std::vector<Point2f> tmp;
//            tmp.push_back(m_point);
//            cornerSubPix( gray, tmp, winSize, Size(-1,-1), termcrit);
//            points[1].push_back(tmp[0]);
//            m_addRemovePt = false;
//        }

//        needToInit = false;
//        imshow("Lucas & Kanade demo", m_currentFrame);
//        char c = (char)waitKey(10);

//        if (c == 27)
//            break;

//        switch( c ) {
//            case 'r':
//                needToInit = true;
//                break;
//            case 'c':
//                points[0].clear();
//                points[1].clear();
//                break;
//            case 'n':
//                nightMode = !nightMode;
//                break;
//        }

//        std::swap(points[1], points[0]);
//        cv::swap(prevGray, gray);
//    }
}

void MainWindow::on_pause_pushButton_clicked()
{
    m_recorder.pause();
}

void MainWindow::on_play_pushButton_clicked()
{
    m_recorder.play();
}

void MainWindow::on_stop_pushButton_clicked()
{
    m_recorder.stop();
}

QImage MainWindow::Mat2QImage(const Mat& src)
{
     Mat temp; // make the same cv::Mat
     cvtColor(src, temp, CV_BGR2RGB); // cvtColor Makes a copt, that what i need
     QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
     dest.bits(); // enforce deep copy, see documentation of QImage::QImage ( const uchar * data, int width, int height, Format format )

     return dest;
}

void MainWindow::on_browse_pushButton_clicked()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(parentWidget(), QObject::tr("File loader"), DATA_DIR, QObject::tr("File loader (*)"));
    if (filename != "") {
         m_filename = filename.toStdString();
    }

    m_recorder.load(m_filename);
    ui->file_lineEdit->setText(filename);
}
