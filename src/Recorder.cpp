#include "Recorder.h"

#include <algorithm>
#include <functional>

Recorder::Recorder()
{

}

Recorder::Recorder(std::string filename)
{
    load(filename);
}

Recorder::~Recorder()
{
    m_cap.release();
    destroyAllWindows();
}

bool Recorder::load(std::string filename)
{
    String videoName(filename);

    m_cap.open(filename);

    if (!m_cap.isOpened()) {
        std::cout << "Could not initialize video capture" << std::endl;
        return false;
    }

    // Store initial frames
    int frame_counter = 0;
    Mat frame;

    while (frame_counter < m_cap.get(CV_CAP_PROP_FRAME_COUNT)) {
        m_cap.read(frame);
        m_frames.emplace_back(frame);
        ++frame_counter;
    }

    if (m_frames.empty()) {
        std::cout << "Video file is empty" << std::endl;
        return false;
    }

    return true;
}

void Recorder::record()
{
    initRecording();

    Mat frame;
    for(;;) {

        // Read the next video frame
        if (m_readState == PLAY || m_readState == STOP) {
            m_cap >> m_currentFrame;
            if (m_currentFrame.empty() || m_readState == STOP)
                break;
        }

        m_currentFrame.copyTo(frame);

        if (m_nightMode)
            frame = Scalar::all(0);

        // Do stuff with the current frame
        processLKOpticalFlow(frame);

        // Display and handle event
        imshow(String("Recording"), frame);
        keyEventHandler((char)waitKey(10));

    }
}

void Recorder::initRecording()
{
    m_cap.set(CV_CAP_PROP_POS_FRAMES, 0);
    m_gray.release();
    m_prevGray.release();
    m_points[0].clear();
    m_points[1].clear();
    m_reloadInit = true;
    m_readState = PLAY;
}

void Recorder::play()
{
    m_readState = PLAY;
}

void Recorder::pause()
{
    m_readState = PAUSE;
}

void Recorder::stop()
{
    m_readState = STOP;
}

void Recorder::keyEventHandler(char key)
{
    switch(key) {
        case 27:
            m_cap.release();
            m_readState = STOP;
            return;
        case 'r':
            m_reloadInit = true;
            break;
        case 'c':
            m_points[0].clear();
            m_points[1].clear();
            break;
        case 'n':
            m_nightMode = !m_nightMode;
            break;
    }
}

void Recorder::processLKOpticalFlow(Mat &frame)
{
    TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);

    cvtColor(frame, m_gray, COLOR_BGR2GRAY);

    if (m_reloadInit)
    {
        // Automatic initialization
        goodFeaturesToTrack(m_gray, m_points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
        cornerSubPix(m_gray, m_points[1], subPixWinSize, Size(-1,-1), termcrit);
        m_addRemovePt = false;

        m_opticalFlow.emplace_back(m_points[1]);
    }
    else if (!m_points[0].empty())
    {
        std::vector<uchar> status;
        std::vector<float> err;

        if (m_prevGray.empty())
            m_gray.copyTo(m_prevGray);

        calcOpticalFlowPyrLK(m_prevGray, m_gray, m_points[0], m_points[1], status, err, winSize,
                             3, termcrit, 0, 0.001);
        size_t i, k;
        for (i = k = 0; i < m_points[1].size(); ++i)
        {
//            if (m_addRemovePt) {
//                if (norm(m_point - m_points[1][i]) <= 5) {
//                    m_addRemovePt = false;
//                    continue;
//                }
//            }
            if (!status[i])
                continue;

            m_points[1][k++] = m_points[1][i];
            circle(frame, m_points[1][i], 3, Scalar(0,255,0), -1, 8);
        }
        m_points[1].resize(k);
    }

    if (m_addRemovePt && m_points[1].size() < (size_t)MAX_COUNT) {
        std::vector<Point2f> tmp;
        tmp.push_back(m_point);
        cornerSubPix( m_gray, tmp, winSize, Size(-1,-1), termcrit);
        m_points[1].push_back(tmp[0]);
        m_addRemovePt = false;
    }

    m_reloadInit = false;

    std::swap(m_points[1], m_points[0]);
    cv::swap(m_prevGray, m_gray);
}

