﻿/*
 * This class handles playback of a video and the computation of the Optical Flow over all its frames
 */


#ifndef RECORDER_H
#define RECORDER_H

#include <iostream>

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>


using namespace cv;

class Recorder
{
public:
    Recorder();
    Recorder(std::string filename);
    ~Recorder();

    bool load(std::string filename);

    // Loop over the frames
    void record(void);
    void initRecording(void);

    void play(void);
    void pause(void);
    void stop(void);

    // Key event handler
    void keyEventHandler(char key);

    void processLKOpticalFlow(Mat& frame);

private:
    VideoCapture m_cap;
    Mat m_currentFrame;
    std::vector<Mat> m_frames;

    // Reading options
    enum ReadState {PLAY=0, PAUSE, STOP, NEXT};
    ReadState m_readState = PLAY;

    const int MAX_COUNT = 500;

    Point2f m_point;
    bool m_addRemovePt = false;


    // Old and new registered points
    std::vector<Point2f> m_points[2];

    // Display options
    bool m_nightMode = false;
    bool m_reloadInit = true;

    /* Lucas & Kanade parameters */
    std::vector<std::vector<Point2f>> m_opticalFlow;
    Mat m_gray, m_prevGray;
};

#endif // RECORDER_H
