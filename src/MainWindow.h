#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include "Recorder.h"


using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_browse_pushButton_clicked();

private slots:
    void on_stop_pushButton_clicked();

private slots:
    void on_play_pushButton_clicked();

private slots:
    void on_pause_pushButton_clicked();

private slots:
    void on_record_pushButton_clicked();

private:
    Ui::MainWindow *ui;

    Recorder m_recorder;
    std::string m_filename;

private:
    QImage Mat2QImage(const Mat& src);
};

#endif // MAINWINDOW_H
